#include "io.h"

enum open_status file_open(FILE** const result_file, const char* const input_file, const char* const mode) {
    *result_file = fopen(input_file, mode);
    if (result_file == NULL) return OPEN_ERROR;
    return OPEN_OK;
}

enum close_status file_close(FILE* const file) {
    int close_status = fclose(file);
    if (close_status != EOF) return CLOSE_OK;
    return CLOSE_ERROR;
}
