#include "rotate.h"
#include <inttypes.h>

struct image rotate_90(const struct image *const image_to_rotate) {
    uint32_t height = image_to_rotate->height;
    uint32_t width = image_to_rotate->width;
    struct pixel *old = image_to_rotate->data;

    struct image result_image = image_create(height, width);
    struct pixel *new = result_image.data;

    for (uint32_t i = 0; i < height; ++i) {
        for (uint32_t j = 0; j < width; ++j) {
            new[j * height + (height - i - 1)] = old[i * width + j];
        }
    }


    return result_image;
}
