#include "bmp_file.h"

#define DEFAULT_BF_TYPE 0x4d42
#define DEFAULT_BI_SIZE 40
#define DEFAULT_X_PELS_PER_METER 2835
#define DEFAULT_Y_PELS_PER_METER 2835
#define DEFAULT_BIT_COUNT 24

uint32_t bmp_get_padding(const uint32_t width) {
    uint32_t padding = width * sizeof(struct pixel) % 4;
    if (padding) {
        return 4 - padding;
    }
    return 0;
}

enum read_status read_header(FILE* in, struct bmp_header *result) {
    size_t header_read_result = fread(result, sizeof(struct bmp_header), 1, in);
    if (header_read_result != 1) 
        return READ_INVALID_HEADER;
    return READ_OK;
}

enum read_status read_data(FILE* in, struct image* const image) {
    uint32_t width = image->width;
    uint32_t padding = bmp_get_padding(width);
    struct pixel* const pixels = image->data;
    for (uint32_t i = 0; i < image->height; ++i) {
        size_t pixels_count = fread(pixels + width * i, sizeof(struct pixel), width, in);
        if (pixels_count != width) {
            return READ_INVALID_BITS;
        }
        int skipped_bytes_count = fseek(in, padding, SEEK_CUR);
        if (skipped_bytes_count != 0) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    enum read_status header_read_res = read_header(in, &header);
    if (header_read_res != READ_OK) {
        return header_read_res;
    }
    *img = image_create(header.biWidth, header.biHeight);
    enum read_status data_read_status = read_data(in, img);
    return data_read_status;
}

static struct bmp_header TEMPLATE_HEADER = {
    .bfType = DEFAULT_BF_TYPE,
    .bfReserved = 0,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = DEFAULT_BI_SIZE,
    .biPlanes = 1,
    .biCompression = 0,
    .biXPelsPerMeter = DEFAULT_X_PELS_PER_METER,
    .biYPelsPerMeter = DEFAULT_Y_PELS_PER_METER,
    .biBitCount = DEFAULT_BIT_COUNT,
    .biClrUsed = 0,
    .biClrImportant = 0
};

enum write_status write_header(FILE *out, const struct image *const img) {
    TEMPLATE_HEADER.biHeight = img->height;
    TEMPLATE_HEADER.biWidth = img->width;
    TEMPLATE_HEADER.biSizeImage = sizeof(struct pixel) * img->width * img->height;
    TEMPLATE_HEADER.bfileSize = TEMPLATE_HEADER.biSizeImage + sizeof(struct bmp_header);

    size_t written_header_bytes_count = fwrite(&TEMPLATE_HEADER, sizeof(struct bmp_header), 1, out);
    if (written_header_bytes_count != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status write_data(FILE *out, const struct image *const img) {
    uint32_t width = (img->width);
    uint32_t padding = bmp_get_padding(width);
    for (uint32_t i = 0; i < img->height; ++i) {
        size_t data_written_count = fwrite(img->data + width * i, sizeof(struct pixel), width, out);
        if (data_written_count != width) {
            return WRITE_ERROR;
        }

        size_t zero_written_count = fwrite(img->data, sizeof(char), padding, out);
        if (zero_written_count != padding) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image* img) {
    enum write_status write_header_status = write_header(out, img);
    if (write_header_status != WRITE_OK) {
        return write_header_status;
    }
    enum write_status write_data_status = write_data(out, img);
    return write_data_status;
}
