#include "bmp_file.h"
#include "image.h"
#include "io.h"
#include "rotate.h"
#include <inttypes.h>
#include <stdio.h>

int main( int argc, char** argv ) {
    
    if (argc != 3) {
        fprintf(stderr, "Wrong number of arguments.\n");
        return -1;
    }
    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    
    FILE* input_file;
    enum open_status open_flag = file_open(&input_file, input_filename, "rb");

    if (open_flag != OPEN_OK) {
        fprintf(stderr, "YA BbICTPEJIUJI CEbE B KOJIEHO cTaTyc: %d\n", open_flag);
        return open_flag;
    }

    struct image img = {0};

    enum read_status read_flag = from_bmp(input_file, &img);
    
    if (read_flag != READ_OK) {
        fprintf(stderr, "YA HE yMeyu 4uTat' cTaTyc: %d \n", read_flag);
        return read_flag;
    }

    struct image rotated_image = rotate_90(&img);

    FILE* output_file;
    open_flag = file_open(&output_file, output_filename, "wb");

    if (open_flag != OPEN_OK) {
        fprintf(stderr, "U3BUHU, HO IIPOU3OIIIJIA OIIIUbKA sTaTyc: %d\n", open_flag);
        return open_flag;
    }
    
    enum write_status write_flag = to_bmp(output_file, &rotated_image);

    image_destroy(img);

    image_destroy(rotated_image);

    if (write_flag != WRITE_OK) {
        fprintf(stderr, "U3BUHU, y HAC ObEDEHHbIU IIEPEPbIB cTaTyc: %d\n", write_flag);
        return write_flag;
    }

    enum close_status close_flag = file_close(output_file);

    if (close_flag != CLOSE_OK) {
        fprintf(stderr, "MbI 3AKPbIBAEMCYA cTaTyc: %d\n", close_flag);
        return close_flag;
    }

    close_flag = file_close(input_file);

     if (close_flag != CLOSE_OK) {
        fprintf(stderr, "MbI HE 3AKPbIBAEMCYA cTaTyc: %d\n", close_flag);
        return close_flag;
    }
    
    return 0;
    
}
