#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
    struct pixel* pixels = malloc(sizeof (struct pixel) * width * height);
    struct image result = (struct image) {.height = height, .width = width, .data = pixels};
    return result;
}

void image_destroy(struct image image) {
    free(image.data);
}
