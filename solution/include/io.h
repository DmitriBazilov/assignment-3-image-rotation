#ifndef IO_H
#define IO_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status file_open(FILE** const result, const char* input, const char* mode);

enum close_status file_close (FILE* const file);

#endif 
