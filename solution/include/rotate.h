#ifndef ROTATE_H
#define ROTATE_H

#include "bmp_file.h"
#include "image.h"
#include "io.h"

struct image rotate_90(const struct image *const image_to_rotate);

#endif // !ROTATE_H
