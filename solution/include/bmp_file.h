#ifndef BMP_FILE_H
#define BMP_FILE_H

#include "bmp_struct.h"
#include "file_status.h"
#include <stdio.h>

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status to_bmp(FILE* out, struct image* img);

#endif // !BMP_FILE_H

